#!/usr/bin/env python

# This script creates an ELB, private security group, launch config and ASG.
# It also bootstraps httpd onto the instance with a default web page
#    that shows the Instance ID and Region of the EC2 intance.

from troposphere import Join, Base64, FindInMap
from troposphere import Parameter, Ref, Tags, Template, GetAtt

from troposphere.ec2 import InternetGateway, SecurityGroupRule, SecurityGroup, VPC, Subnet, VPCGatewayAttachment, RouteTable, Route, SubnetRouteTableAssociation
from troposphere.autoscaling import AutoScalingGroup, Tag, LaunchConfiguration
from troposphere.iam import PolicyType, Role, InstanceProfile

def create_template(name):
    # Parameters:
    #     name = Name tag to be applied to resources
    t = Template()
    t.add_description(name + ' Template - VPC, subnet, IGW, RT.')
    ref_region = Ref('AWS::Region')

    ####################
    #### Parameters ####
    ####################
    OwnerEmailParam = t.add_parameter(
        Parameter(
            "OwnerEmailParam",
            Type = "String",
            Description = "Your resources will be tagged with this email address as the owner.",
            Default = "connor.williams@cloudreach.com",
            AllowedPattern = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
        )
    )
    MyIpParam = t.add_parameter(
        Parameter(
            "MyIpParam",
            Type = "String",
            Description = "What is your IP address?",
            Default = "87.213.54.9/32",
        )
    )
    KeyPairParam = t.add_parameter(
        Parameter(
            "KeyPairParam",
            Type = "AWS::EC2::KeyPair::KeyName",
            Description = "Your private key pair name.",
            Default = "connor.williams"
        )
    )

    ##################
    #### Mappings ####
    ##################
    t.add_mapping('IpRangeMap', {
        "Prod":{
            "vpcCIDR": "10.0.0.0/16",
            "subnetCIDR": "10.0.1.0/24"
        }
    })


    ###################
    #### Resources ####
    ###################
    defaultTags = Tags(
        Name="homeMade",
        Owner=Ref(OwnerEmailParam)
    )

    Vpc = t.add_resource(
        VPC(
            name+"Vpc",
            CidrBlock=FindInMap("IpRangeMap", "Prod", "vpcCIDR"),
            Tags=defaultTags
        )
    )

    AsgSubnet = t.add_resource(
        Subnet(
            name+'Subnet',
            VpcId=Ref(Vpc),
            CidrBlock=FindInMap("IpRangeMap", "Prod", "subnetCIDR"),
            AvailabilityZone='eu-west-1a',
            Tags=defaultTags
        )
    )

    InternetGW = t.add_resource(
        InternetGateway(
            name+'IGW',
            Tags=defaultTags
        )
    )

    AttachGW = t.add_resource(
        VPCGatewayAttachment(
            name+'AttachGW',
            VpcId=Ref(Vpc),
            InternetGatewayId=Ref(InternetGW)
        )
    )

    RouteTable1 = t.add_resource(
        RouteTable(
            name+'RT',
            VpcId=Ref(Vpc),
            Tags=defaultTags
        )
    )

    Route1 = t.add_resource(
        Route(
            name+"route1",
            RouteTableId=Ref(RouteTable1),
            DestinationCidrBlock="0.0.0.0/0",
            GatewayId=Ref(InternetGW)
        )
    )

    SubnetRTAssociation = t.add_resource(
        SubnetRouteTableAssociation(
            name+'RTAssocition',
            SubnetId=Ref(AsgSubnet),
            RouteTableId=Ref(RouteTable1)
        )
    )

    print(t.to_json())

if __name__ == "__main__":
    stack = create_template('homeMade')
