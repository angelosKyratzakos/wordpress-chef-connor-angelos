#!/usr/bin/env python

# This script creates an ELB, private security group, launch config and ASG.
# It also bootstraps httpd onto the instance with a default web page
#    that shows the Instance ID and Region of the EC2 intance.

from troposphere import Join, Base64, FindInMap
from troposphere import Parameter, Ref, Tags, Template, GetAtt

from troposphere.ec2 import InternetGateway, SecurityGroupRule, SecurityGroup, VPC, Subnet
from troposphere.autoscaling import AutoScalingGroup, Tag, LaunchConfiguration
from troposphere.iam import PolicyType, Role, InstanceProfile

def create_template(name):
    # Parameters:
    #     name = Name tag to be applied to resources
    t = Template()
    t.add_description(name + ' Template - Security group, launch config, autoscaling group.')
    ref_region = Ref('AWS::Region')

    ####################
    #### Parameters ####
    ####################
    OwnerEmailParam = t.add_parameter(
        Parameter(
            "OwnerEmailParam",
            Type = "String",
            Description = "Your resources will be tagged with this email address as the owner.",
            Default = "connor.williams@cloudreach.com",
            AllowedPattern = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
        )
    )
    VpcIdParam = t.add_parameter(
        Parameter(
            "VpcIdParam",
            Type = "AWS::EC2::VPC::Id",
            Description = "The VPC ID where you want the app to be deployed.",
        )
    )
    MyIpParam = t.add_parameter(
        Parameter(
            "MyIpParam",
            Type = "String",
            Description = "What is your IP address?",
            Default = "87.213.54.9/32",
        )
    )
    KeyPairParam = t.add_parameter(
        Parameter(
            "KeyPairParam",
            Type = "AWS::EC2::KeyPair::KeyName",
            Description = "Your private key pair name.",
            Default = "connor.williams"
        )
    )
    AsgSubnetsParam = t.add_parameter(
        Parameter(
            "AsgSubnetsParam",
            Type = "List<AWS::EC2::Subnet::Id>",
            Description = "Subnets for the ASG to be in.",
        )
    )


    ###################
    #### Resources ####
    ###################
    defaultTags = Tags(
        Owner=Ref(OwnerEmailParam)
    )

    AsgSg = t.add_resource(
        SecurityGroup(
            name + 'AsgSg',
            VpcId=Ref(VpcIdParam),
            GroupDescription='Security group for ASG.',
            SecurityGroupIngress=[
                SecurityGroupRule(
                    ToPort='22',
                    FromPort='22',
                    IpProtocol='tcp',
                    CidrIp=Ref(MyIpParam)
                ),
                SecurityGroupRule(
                    ToPort='80',
                    FromPort='80',
                    IpProtocol='tcp',
                    CidrIp=Ref(MyIpParam)
                )
            ],
            Tags=defaultTags
        )
    )


    IamRole = t.add_resource(
        Role(
            name + 'IamRole',
            AssumeRolePolicyDocument={
               "Version" : "2012-10-17",
               "Statement": [ {
                  "Effect": "Allow",
                  "Principal": {
                     "Service": [ "ec2.amazonaws.com" ]
                  },
                  "Action": [ "sts:AssumeRole" ]
               } ]
            },
            Path="/"
        )
    )

    IamPolicy = t.add_resource(
        PolicyType(
            name+"IamPolicy",
            PolicyName="readMyS3Bucket",
            PolicyDocument={
                "Version": "2012-10-17",
                "Statement": [
                    {
                        "Effect": "Allow",
                        "Action": [
                            "s3:Get*",
                            "s3:List*"
                        ],
                        "Resource": "*"
                    }
                ]
            },
            Roles=[Ref(IamRole)]
        )
    )

    IamInstanceProfile = t.add_resource(
        InstanceProfile(
            name+'InstanceProfile',
            Path='/',
            Roles=[Ref(IamRole)]
        )
    )

    ASGLaunchConfig_logical = name + 'ASGLaunchConfig'
    ASGLaunchConfig = t.add_resource(
        LaunchConfiguration(
            ASGLaunchConfig_logical,
            ImageId="ami-ed82e39e", # Ubuntu=ami-ed82e39e, AwsLinux=ami-d41d58a7
            InstanceMonitoring=False,
            AssociatePublicIpAddress=True,
            InstanceType="t2.micro",
            IamInstanceProfile=Ref(IamInstanceProfile),
            SecurityGroups=[Ref(AsgSg)],
            KeyName=Ref(KeyPairParam),
            UserData=Base64(Join("",
                [
                    "#!/bin/bash -xe\n",
                    "sudo apt-get update -y\n",
                    "sudo apt-get upgrade -y\n",
                    "sudo apt-get --assume-yes install awscli\n",
                    "curl -L https://www.opscode.com/chef/install.sh | bash\n",
                    "mkdir /etc/chef\n",
                    "mkdir /etc/chef/cache\n",
                    "aws s3 sync s3://connor-angelos-chefting /etc/chef --region eu-west-1 && tar -xvzf /etc/chef/cookbooks.tar.gz -C /etc/chef\n",
                    "echo \"* * * * * ec2-user aws s3 sync s3://connor-angelos-chefting /etc/chef --region eu-west-1 && tar -xvzf /etc/chef/cookbooks.tar.gz -C /etc/chef\" >> /etc/crontab\n",
                    "sudo usermod -aG root www-data\n",
                    "chef-solo -c /etc/chef/solo.rb -j /etc/chef/node.json\n"
                ]
            ))
        )
    )

    WebServerASG = t.add_resource(
        AutoScalingGroup(
            name + 'WebServerASG',
            LaunchConfigurationName=Ref(ASGLaunchConfig),
            MinSize='1',
            DesiredCapacity='1',
            Cooldown='1',
            MaxSize='1',
            VPCZoneIdentifier=Ref(AsgSubnetsParam),
            Tags=[
                Tag("Name", name+'-WebServerASG', False),
                Tag("Owner", Ref(OwnerEmailParam), True)
            ]
        )
    )

    print(t.to_json())

if __name__ == "__main__":
    stack = create_template('homeMade')
