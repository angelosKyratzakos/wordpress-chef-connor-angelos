force_default['wordpress']['db']['name'] = 'homemade'  # Name of the WordPress MySQL database.
force_default['wordpress']['db']['user'] = 'homemade' # Name of the WordPress MySQL user.
force_default['wordpress']['db']['pass'] = 'homemade' # Password of the WordPress MySQL user. By default, generated using openssl cookbook.
force_default['wordpress']['db']['port'] = 3306 # Port of the WordPress MySQL database.
# mysql -h localhost -P 3306 -u homemade -p
